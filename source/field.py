#! /usr/bin/env python

import pygtk
import gtk
import random
import gobject

import hiscores
import square
import common

class Field:
    """The field of a minesweeper game"""
    
    def __init__(self, menu, width, height, amountOfMines):
        """Field constructor"""
        self.theme         = common.getTheme()
        self.menu          = menu
        self.width         = width
        self.height        = height
        self.amountOfMines = amountOfMines
        self.opened        = 0
        self.flags         = 0
        self.started       = False
        self.finished      = False
        self.time          = 0
        self.field         = []
        self.frame         = gtk.AspectFrame(None, 0.5, 0, float(width) / float(height), False)
        self.frame.set_shadow_type(gtk.SHADOW_NONE)
        if width == height == 9 and amountOfMines == 10:
            self.difficulty = "easy"
        elif width == height == 16 and amountOfMines == 40:
            self.difficulty = "medium"
        elif ((width == 30 and height == 16) or (width == 16 and height == 30)) and amountOfMines == 99:
            self.difficulty = "hard"
        else:
            self.difficulty = "Custom"
        
        # place random mines
        mines     = []
        tempMines = [True] * amountOfMines + [False] * (width * height - amountOfMines)
        random.shuffle(tempMines)
        for i in range(height):
            mines.append(tempMines[i * width : (i + 1) * width])
        
        # create field
        table = gtk.Table(height, width, True)
        for j in range(height):
            line = []
            for i in range(width):
                current = square.Square(self, mines[j][i], i, j)
                line.append(current)
                table.attach(current.button, i, i + 1, j, j + 1)
            self.field.append(line)
        self.frame.add(table)
        
        # count the surrounding mines of each field            
        for j in range(height):
            for i in range(width):
                if self.field[j][i].mine:
                    for b in range(j - 1, j + 2):
                        for a in range (i - 1, i + 2):
                            if 0 <= b < height and 0 <= a < width:
                                self.field[b][a].surroundingMines += 1
    
    def printField(self):
        """Print the field to the terminal"""
        for j in self.field:
            for i in j:
                if i.mine:
                    print "X",
                elif i.surroundingMines == 0:
                    print ".",
                else:
                    print str(i.surroundingMines),
            print ""
    
    def openSurrounding(self, x, y):
        """Open the fields surrounding a square"""
        for j in range(y - 1, y + 2):
            for i in range(x - 1, x + 2):
                if 0 <= j < self.height and 0 <= i < self.width and not self.field[j][i].opened and not self.field[j][i].flag:
                    self.field[j][i].openSquare()
    
    def setFlags(self, x, y, flag):
        """Open the fields surrounding a square"""
        if flag:
            self.flags += 1
        else:
            self.flags -= 1
        self.updateLabel()
        for j in range(y - 1, y + 2):
            for i in range(x - 1, x + 2):
                if 0 <= j < self.height and 0 <= i < self.width and not (j == y and i == x):
                    if flag:
                        self.field[j][i].surroundingFlags += 1
                    else:
                        self.field[j][i].surroundingFlags -= 1
    
    def startTimer(self):
        """Start the game timer"""
        self.started = True
        gobject.timeout_add_seconds(1, self.incrementTimer)
    
    def incrementTimer(self):
        """Increment the timer by 1"""
        if self.started and not self.finished:
            self.time += 1
            self.updateLabel()
        return self.started and not self.finished
    
    def updateLabel(self):
        """Update the score label in the menu"""
        self.menu.score.set_label("%d/%d Time: %d" % (self.flags, self.amountOfMines, self.time))
    
    def updateTheme(self):
        """Update the colors and icons of all squares"""
        self.theme = common.getTheme()
        for y in self.field:
            for x in y:
                if x.opened:
                    x.setColor(self.theme[3])
                elif x.flag:
                    x.setColor(self.theme[2])
                else:
                    x.setColor(self.theme[4])
    
    def win(self):
        """End the game and show the hiscores menu"""
        self.finished = True
        hiscores.show(self.menu, difficulty = self.difficulty, score = self.time)
    
    def lose(self):
        """Make all mines explode and start a new game"""
        self.finished = True
        self.frame.set_sensitive(False)
        print "BOOM! Haha you lost! Noob..."
        for y in self.field:
            for x in y:
                if x.mine and not x.flag and not x.opened:
                    x.reveal()


