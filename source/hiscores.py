#! /usr/bin/env python

import pygtk
import gtk
import os
import common

class Hiscores:
    """Show the hiscores"""
    
    def __init__(self, difficulty = "easy", score = 9999999):
        """Class constructor"""
        self.difficulty = difficulty
        self.scores     = []
        self.addScore   = False
        self.newScore   = score
        self.written    = False
        with open("%s/settings/hiscores" % common.path) as f:
            for line in f:
                text = line.rstrip("\n").split(",")
                text[1] = int(text[1])
                self.scores.append(text)
                if difficulty == text[0] and self.newScore < text[1]:
                    self.addScore = True
        
        notebook = gtk.Notebook()
        notebook.append_page(self.getHiscores("easy")  , gtk.Label("easy"))
        notebook.append_page(self.getHiscores("normal"), gtk.Label("normal"))
        notebook.append_page(self.getHiscores("hard")  , gtk.Label("hard"))
        
        box = gtk.VBox()
        box.pack_start(notebook)
        
        if self.addScore:
            self.entry        = gtk.Entry()
            self.submitButton = gtk.Button("Submit score")
            hBox              = gtk.HBox()
            congratulations   = gtk.Label("Congratulations!\nYou beat the hiscore for the %s difficulty!" % difficulty)
            congratulations.set_justify(gtk.JUSTIFY_CENTER)
            self.submitButton.connect("clicked", self.writeScore)
            hBox.pack_start(self.entry)
            hBox.pack_start(self.submitButton, True, False)
            box.pack_start(congratulations)
            box.pack_start(hBox, True, False)
        
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.add(box)
        self.window.set_title("Hiscores")
        self.window.set_border_width(10)
        self.window.show_all()
    
    def getHiscores(self, difficulty):
        """Return a table containing the hiscores"""
        i     = 0
        table = gtk.Table(3, 10)
        for current in self.scores:
            if current[0] == difficulty:
                table.attach(gtk.Label(i + 1)     , 0, 1, i, i + 1)
                table.attach(gtk.Label(current[1]), 1, 2, i, i + 1)
                table.attach(gtk.Label(current[2]), 2, 3, i, i + 1)
                i += 1
        return table
    
    def writeScore(self, widget):
        """Write the score to a file"""
        if not self.written:
            i = 0
            for score in self.scores:
                if score[0] == self.difficulty:
                    if self.newScore < score[1] and not self.written:
                        self.scores.insert(i, [self.difficulty, self.newScore, self.entry.get_text()])
                        self.written = True
                    elif self.written and i % 10 == 0:
                        self.scores.pop(i)
                        break
                i += 1
            os.remove("%s/settings/hiscores" % common.path)
            with open("%s/settings/hiscores" % common.path, "w") as f:
                for score in self.scores:
                     f.write("%s,%d,%s\n" % (score[0], score[1], score[2]))

def show(difficulty = "easy", score = 9999999):
    """Create and display a hiscores screen"""
    h = Hiscores(difficulty, score)


