#! /usr/bin/env python

import pygtk
import gtk
import os
import common
import menu
class Options:
    """Options menu"""
    
    def __init__(self, menu):
        """Options menu constructor"""
        with open("%s/settings/options" % common.path) as f:
            self.saved    = map(lambda x: int(x), f.read().rstrip("\n").split(","))
        self.menu         = menu
        self.radioButtons = gtk.VBox()
        self.spinButtons  = gtk.VBox()
        vBox              = gtk.VBox()
        hBox              = gtk.HBox()
        button            = gtk.Button("save")
        width             = int(self.saved[0])
        height            = int(self.saved[1])
        amountOfMines     = int(self.saved[2])
        self.buttonEasy   = self.createRadioButton("Easy")
        self.buttonMedium = self.createRadioButton("Medium", self.buttonEasy)
        self.buttonHard   = self.createRadioButton("Hard"  , self.buttonEasy)
        self.buttonCustom = self.createRadioButton("Custom", self.buttonEasy)
        self.spinWidth    = self.createSpinButton("Width" , 30, width)
        self.spinHeight   = self.createSpinButton("Height", 30, height)
        self.spinMines    = self.createSpinButton("Mines" , self.spinWidth.get_value_as_int() * self.spinHeight.get_value_as_int() - 1, amountOfMines)
        
        button.connect("clicked", self.save)
        hBox.pack_start(self.radioButtons)
        hBox.pack_start(self.spinButtons)
        vBox.pack_start(hBox)
        vBox.pack_start(button, False, False)
        
        self.spinWidth.connect("value-changed", lambda x: self.spinMines.set_range(0, self.spinWidth.get_value_as_int() * self.spinHeight.get_value_as_int() - 1))
        self.spinHeight.connect("value-changed", lambda x: self.spinMines.set_range(0, self.spinWidth.get_value_as_int() * self.spinHeight.get_value_as_int() - 1))
        
        if width == height == 9 and amountOfMines == 10:
            self.buttonEasy.set_active(True)
        elif width == height == 16 and amountOfMines == 40:
            self.buttonMedium.set_active(True)
        elif ((width == 30 and height == 16) or (width == 16 and height == 30)) and amountOfMines == 99:
            self.buttonHard.set_active(True)
        else:
            self.buttonCustom.set_active(True)
            
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.add(vBox)
        self.window.set_title("Options")
        self.window.set_border_width(10)
        self.window.show_all()

    def createRadioButton(self, label = None, group = None):
        """Create a new radiobutton"""
        radioButton = gtk.RadioButton(group, label)
        radioButton.connect("toggled", self.toggled, label)
        self.radioButtons.pack_start(radioButton)
        return radioButton
    
    def createSpinButton(self, label, upper, current):
        """Create a new spinbutton"""
        spinButton = gtk.SpinButton(gtk.Adjustment(current, 1, upper, 1, 1))
        spinButton.set_numeric(True)
        spinButton.connect("value-changed", self.setDifficulty)
        self.spinButtons.pack_start(spinButton)
        return spinButton
    
    def setDifficulty(self, widget):
        """Change the selected radiobutton according to the selected values"""
        self.spinMines.set_range(0, self.spinWidth.get_value_as_int() * self.spinHeight.get_value_as_int() - 1)
        self.spinMines.set_range(0, self.spinWidth.get_value_as_int() * self.spinHeight.get_value_as_int() - 1)
        if self.spinWidth.get_value_as_int() == self.spinHeight.get_value_as_int() == 9 and self.spinMines.get_value_as_int() == 10:
            self.buttonEasy.set_active(True)
        elif self.spinWidth.get_value_as_int() == self.spinHeight.get_value_as_int() == 16 and self.spinMines.get_value_as_int() == 40:
            self.buttonMedium.set_active(True)
        elif ((self.spinWidth.get_value_as_int() == 16 and self.spinHeight.get_value_as_int() == 30) or (self.spinWidth.get_value_as_int() == 30 and self.spinHeight.get_value_as_int() == 16)) and self.spinMines.get_value_as_int() == 99:
            self.buttonHard.set_active(True)
        else:
            self.buttonCustom.set_active(True)
    
    def toggled(self, widget, selected):
        """Callback for radiobuttons"""
        if widget.get_active():
            if selected == "Easy":
                self.spinWidth.set_value(9)
                self.spinHeight.set_value(9)
                self.spinMines.set_value(10)
            elif selected == "Medium":
                self.spinWidth.set_value(16)
                self.spinHeight.set_value(16)
                self.spinMines.set_value(40)
            elif selected == "Hard":
                self.spinWidth.set_value(30)
                self.spinHeight.set_value(16)
                self.spinMines.set_value(99)
    
    def save(self, widget):
        """Save the options to a file"""
        os.remove("%s/settings/options" % common.path)
        with open ("%s/settings/options" % common.path, "w") as f:
            f.write("%d,%d,%d,%d" % (self.spinWidth.get_value_as_int(), self.spinHeight.get_value_as_int(), self.spinMines.get_value_as_int(), self.saved[3]))
        self.window.destroy()
        menu.menu.newGame()

def show(menu):
    """Create and display an options screen"""
    o = Options(menu)


