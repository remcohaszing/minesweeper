#! /usr/bin/env python

import pygtk
import gtk
import os

import field
import common

class Theme:
    """Theme selector menu"""
    
    def __init__(self, menu):
        """Theme selector menu constructor"""
        self.saved        = common.getTheme()
        self.menu         = menu
        vbox              = gtk.VBox()
        self.table        = gtk.Table(4, 2, True)
        self.mineButton   = self.colorSelector("Mine"  , 0, 1)
        self.flagButton   = self.colorSelector("Flag"  , 1, 2)
        self.openButton   = self.colorSelector("Opened", 2, 3)
        self.closedButton = self.colorSelector("Closed", 3, 4)
        combobox          = gtk.combo_box_new_text()
        saveButton        = gtk.Button("Save")
        for dirname, dirnames, filenames in os.walk("%s/themes" % menu.path):
            dirnames.sort()
            j = 0
            for i in dirnames:
                combobox.append_text(i)
                if self.saved[0] == i:
                    combobox.set_active(j)
                j += 1
        combobox.connect("changed", self.comboboxSelected)
        saveButton.connect("clicked", self.save)
        vbox.pack_start(self.table)
        vbox.pack_start(combobox)
        vbox.pack_start(saveButton)
        
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.add(vbox)
        self.window.set_title("Options")
        self.window.set_border_width(10)
        self.window.show_all()
        self.loadImages()
    
    def colorSelector(self, text, place, save):
        """Create a color selector button"""
        button = gtk.Button("")
        frame  = gtk.AspectFrame(ratio = 1, obey_child = False)
        label  = gtk.Label(text)
        button.modify_bg(gtk.STATE_NORMAL,   gtk.gdk.Color(self.saved[save]))
        button.modify_bg(gtk.STATE_ACTIVE,   gtk.gdk.Color(self.saved[save]))
        button.modify_bg(gtk.STATE_SELECTED, gtk.gdk.Color(self.saved[save]))
        button.modify_bg(gtk.STATE_PRELIGHT, gtk.gdk.Color(self.saved[save]))
        button.connect("clicked", self.showColorSelector, save)
        frame.set_shadow_type(gtk.SHADOW_NONE)
        frame.add(button)
        self.table.attach(label, 0, 1, place, place + 1)
        self.table.attach(frame, 1, 2, place, place + 1)
        return button
    
    def showColorSelector(self, widget, save):
        """Show a Color Selector Dialog"""
        colorselDialog = gtk.ColorSelectionDialog("Select a color")
        colorsel       = colorselDialog.colorsel
        colorsel.set_previous_color(gtk.gdk.Color(self.saved[save]))
        colorsel.set_current_color(gtk.gdk.Color(self.saved[save]))
        colorselDialog.show()
        if colorselDialog.run() -- gtk.RESPONSE_OK:
            self.saved[save] = str(colorsel.get_current_color())
            widget.modify_bg(gtk.STATE_NORMAL,   colorsel.get_current_color())
            widget.modify_bg(gtk.STATE_ACTIVE,   colorsel.get_current_color())
            widget.modify_bg(gtk.STATE_SELECTED, colorsel.get_current_color())
            widget.modify_bg(gtk.STATE_PRELIGHT, colorsel.get_current_color())
            colorselDialog.hide()
    
    def comboboxSelected(self, widget):
        """Change the saved state when selecting another theme"""
        self.saved[0] = widget.get_active_text()
        self.loadImages()
    
    def loadImages(self):
        """Load the images"""
        self.mineButton.remove(self.mineButton.get_children()[0])
        self.flagButton.remove(self.flagButton.get_children()[0])
        self.openButton.remove(self.openButton.get_children()[0])
        allocation = self.mineButton.get_allocation()
        try:
            pixbuf = common.getPicture("mine")
            image  = gtk.image_new_from_pixbuf(pixbuf.scale_simple(int(allocation.width * 0.7), int(allocation.height * 0.7), gtk.gdk.INTERP_BILINEAR))
            image.show()
            self.mineButton.add(image)
        except:
            self.mineButton.set_label("x.x")
        try:
            pixbuf = common.getPicture("flag")
            image  = gtk.image_new_from_pixbuf(pixbuf.scale_simple(int(allocation.width * 0.7), int(allocation.height * 0.7), gtk.gdk.INTERP_BILINEAR))
            image.show()
            self.flagButton.add(image)
        except:
            self.flagButton.set_label("\o/")
        try:
            pixbuf = common.getPicture("1")
            image  = gtk.image_new_from_pixbuf(pixbuf.scale_simple(int(allocation.width * 0.7), int(allocation.height * 0.7), gtk.gdk.INTERP_BILINEAR))
            image.show()
            self.openButton.add(image)
        except:
            self.openButton.set_label("1")
    
    def save(self, widget):
        """Save the options to a file"""
        os.remove("%s/settings/theme" % self.menu.path)
        string = ""
        for i in self.saved:
            string += "%s," % i
        with open ("%s/settings/theme" % self.menu.path, "w") as f:
            f.write(string.rstrip(","))
        common.setTheme(self.saved)
        self.menu.game.updateTheme()
        self.window.destroy()

def show(menu):
    """Create and display an options screen"""
    t = Theme(menu)


