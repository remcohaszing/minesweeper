#! /usr/bin/env python

import pygtk
pygtk.require('2.0')
import gtk
import os
import sys

import field
import hiscores
import options
import theme

class Menu:
    """The user Menu"""
    
    def __init__(self):
        """The interface constructor"""
        self.path = os.path.abspath(os.path.dirname(__file__)) + "/.."
        with open("%s/settings/options" % self.path) as f:
            if f.read().rstrip("\n").split(",")[3] == "0":
                smile = False
            else:
                smile = True
        self.score       = gtk.MenuItem("")
        self.frame       = gtk.VBox(False, 0)
        self.smileyFrame = gtk.AspectFrame(obey_child = False)
        smileyButton     = gtk.Button(":)")
        menu_bar         = gtk.MenuBar()
        menu             = gtk.Menu()
        root_menu        = gtk.MenuItem("File")
        displaySmiley    = gtk.CheckMenuItem("Show smiley")
        agr              = gtk.AccelGroup()
        window           = gtk.Window(gtk.WINDOW_TOPLEVEL)
        root_menu.set_submenu(menu)
        menu_bar.append(root_menu)
        displaySmiley.set_active(smile)
        displaySmiley.connect("toggled", self.toggleSmiley)
        self.createMenuItem(menu, "New game", "N", agr, self.selectNewGame)
        self.createMenuItem(menu, "Hiscores", "H", agr, lambda x: hiscores.show(self))
        self.createMenuItem(menu, "Options" , "O", agr, lambda x: options.show(self))
        self.createMenuItem(menu, "Theme"   , "T", agr, lambda x: theme.show(self))
        menu.append(displaySmiley)
        self.createMenuItem(menu, "Quit"    , "Q", agr, lambda x: gtk.main_quit())
        self.score.set_right_justified(True)
        menu_bar.append(self.score)
        
        self.smileyFrame.add(smileyButton)
        self.smileyFrame.set_size_request(60, 60)
        self.frame.pack_start(menu_bar, False, False, 2)
        self.frame.pack_start(self.smileyFrame, False, False, 10)
        self.newGame()
        
        window.add_accel_group(agr)
        window.add(self.frame)
        window.connect("destroy", lambda x: gtk.main_quit())
        window.set_title("Minesweeper")
        window.resize(self.game.width * 40, 30 + self.game.height * 40)
        window.set_position(gtk.WIN_POS_CENTER)
        window.show_all()
        if not displaySmiley.get_active():
            self.smileyFrame.hide()
    
    def createMenuItem(self, menu, label, token, agr, activate):
        """Create a menu item in the File menu"""
        key, mod = gtk.accelerator_parse("<Control>%s" % token)
        item     = gtk.MenuItem(label)
        item.connect("activate", activate)
        item.add_accelerator("activate", agr, key, mod, gtk.ACCEL_VISIBLE)
        menu.append(item)
    
    def selectNewGame(self, widget):
        """Callback for New Game menu item"""
        self.newGame()
    
    def toggleSmiley(self, widget):
        """Hide or show the smiley button"""
        if widget.get_active():
            self.smileyFrame.show()
        else:
            self.smileyFrame.hide()
    
    def newGame(self):
        """Create a new game"""
        try:
            self.frame.remove(self.game.frame)
            self.game.finished = True
            del self.game
        except: pass
        with open("%s/settings/options" % self.path) as f:
            saved = map(lambda x: int(x), f.read().rstrip("\n").split(","))
        self.score.set_label("%d/%d Time: %d" % (0, saved[2], 0))
        self.game = field.Field(self, saved[0], saved[1], saved[2])
        self.frame.pack_start(self.game.frame)
        self.frame.show_all()

def main():
    menu = Menu()
    gtk.main()


