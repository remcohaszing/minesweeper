#! /usr/bin/env python

import pygtk
import gtk
import field
import common

class Square():
    """A single square in the minefield"""

    def __init__(self, field, mine, x, y):
        """Square constructor"""
        self.x                = x
        self.y                = y
        self.mine             = mine
        self.field            = field
        self.opened           = False
        self.flag             = False
        self.surroundingMines = 0
        self.surroundingFlags = 0
        self.pixbuf           = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB, False, 8, 1, 1)
        self.button           = gtk.Button()
        self.button.connect("button-press-event", self.click)
        self.button.connect("size-allocate", self.reload)
        self.setColor(self.field.theme[4])

    def click(self, widget, event):
        """Event handler for the button"""
        if event.button == 1 and not self.flag:
            if not self.opened:
                self.openSquare()
            elif self.surroundingMines == self.surroundingFlags:
                self.field.openSurrounding(self.x, self.y)
        elif event.button == 2:
            pass # todo
        elif event.button == 3 and not self.opened:
            if self.flag:
                self.flag = False
                self.field.setFlags(self.x, self.y, False)
                self.button.remove(self.button.get_children()[0])
                self.setColor(self.field.theme[4])
            else:
                self.flag = True
                self.field.setFlags(self.x, self.y, True)
                try:
                    self.pixbuf = common.getPictureFlag()
                    self.reload(widget, event)
                    self.image.show()
                    self.button.add(self.image)
                except:
                    self.button.set_label("\o/")
                self.setColor(self.field.theme[2])
        if not self.field.started and not self.field.finished:
            self.field.started = True
            self.field.startTimer()

    def reload(self, widget, event):
        """reload the containing image"""
        allocation = widget.get_allocation()
        self.image = gtk.image_new_from_pixbuf(self.pixbuf.scale_simple(int(allocation.width * 0.7), int(allocation.height * 0.7), gtk.gdk.INTERP_BILINEAR))

    def openSquare(self):
        """Open the square"""
        self.opened = True
        if self.mine:
            self.button.modify_bg(gtk.STATE_INSENSITIVE, gtk.gdk.Color(self.field.theme[1]))
            try:
                self.pixbuf = common.getPictureBoom()
                self.reload(self.button, "size-allocate")
                self.image.show()
                self.button.add(self.image)
            except:
                self.button.set_label("X.X")
            self.field.lose()
        else:
            self.field.opened += 1
            self.setColor(self.field.theme[3])
            if self.surroundingMines > 0:
                self.button.set_label(str(self.surroundingMines))
            if self.field.width * self.field.height == self.field.opened + self.field.amountOfMines:
                self.field.win()
            if self.surroundingMines == 0:
                self.field.openSurrounding(self.x, self.y)

    def reveal(self):
        """Reveal wether there is a mine or not"""
        self.button.modify_bg(gtk.STATE_INSENSITIVE, gtk.gdk.Color(self.field.theme[1]))
        try:
            self.pixbuf = common.getPictureMine()
            self.reload(self.button, "")
            self.image.show()
            self.button.add(self.image)
        except:
            self.button.set_label("x.x")

    def setColor(self, color):
        """Set the color of the button"""
        self.button.modify_bg(gtk.STATE_NORMAL,   gtk.gdk.Color(color))
        self.button.modify_bg(gtk.STATE_ACTIVE,   gtk.gdk.Color(color))
        self.button.modify_bg(gtk.STATE_SELECTED, gtk.gdk.Color(color))
        self.button.modify_bg(gtk.STATE_PRELIGHT, gtk.gdk.Color(color))
