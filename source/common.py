#! /usr/bin/env python

import gtk
import pygtk
import os

def getPicture(name):
    "Return a pixbuf from a file"
    try:
        return gtk.gdk.pixbuf_new_from_file("%s/themes/%s/%s.png" % (path, theme[0], name))
    except:
        try:
            return gtk.gdk.pixbuf_new_from_file("%s/themes/%s/%s.jpg" % (path, theme[0], name))
        except:
            try:
                return gtk.gdk.pixbuf_new_from_file("%s/themes/%s/%s.svg" % (path, theme[0], name))
            except:
                try:
                    return gtk.gdk.pixbuf_new_from_file("%s/themes/%s/%s.xmp" % (path, theme[0], name))
                except:
                    try:
                        return gtk.gdk.pixbuf_new_from_file("%s/themes/%s/%s.bmp" % (path, theme[0], name))
                    except:
                        return None

def getTheme():
    """Return the current theme"""
    return theme

def setTheme(new):
    """Set a new theme"""
    theme = new

def getPictureMine():
    """Return a pixbuf of a mine picture"""
    return pixMine

def getPictureFlag():
    """Return a pixbuf of a flag picture"""
    return pixFlag

def getPictureBoom():
    """Return a pixbuf of a boom picture"""
    return pixBoom

path = os.path.abspath(os.path.dirname(__file__)) + "/.."
with open("%s/settings/theme" % path) as f:
    theme = f.read().rstrip("\n").split(",")
pixMine = getPicture("mine")
pixFlag = getPicture("flag")
pixBoom = getPicture("pixBoom")
